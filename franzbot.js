//get our api key from config.json

const config = require("./config.json");
var apikey = config.key;
var paruserverurl = config.parurebooturl;

const Discord = require("discord.js");

const client = new Discord.Client();
const prefix = "!";

//const badwords = /\b(torrent|pirat|free copy|free copies|download)+.*\b(ffxiv|client|1\.[0-9]{0,2}[a|b|c]?)+/gi;

const badword1 = /(ffxiv|1\.[0-9]{0,2}[a|b|c]?)+(?!.*\1)/ig;
const badword2 = /(torrent|pirat|free|copy|copies|download|ISO)+(?!.*\1)/ig;
const goodwords = /(can't|don't|cannot|ARR|HW|SB|SHB|trial|[2-9]\.{0,2})+/ig;

client.on("ready", () => {
    console.log("Ready");
});

client.on("message", (message) => {

    //ignore messages from the bot
    if( message.author.bot) return;
	
	goodwordweight = 0;
	hadbadword1 = false;
	hadbadword2 = 0;

	wordset = new Set();
	
	results1 = message.content.match(badword1);
	if (results1 != null)
	{
		results1.forEach( (result) =>
	//while (result =  badword1.exec(message.content) )
	{	
			
			console.log("Matched a badword for client: " + result);
			hadbadword1 = true;
			wordset.add(result);
	}
	);
	}

	results2 = message.content.match(badword2);
        if (results2 != null)
        {
                results2.forEach( (result) =>
        //while (result =  badword1.exec(message.content) )
        {

                        console.log("Matched a badword for obtain: " + result);
                        hadbadword2++;
			wordset.add(result);
        }
        );
        }
	
	results3 = message.content.match(goodwords);
        if (results3 != null)
        {
                results3.forEach( (result) =>
        //while (result =  badword1.exec(message.content) )
        {

                        console.log("Matched a badword for client: " + result);
			goodwordweight++;
        }
        );
        }
	
	console.log("Bad wordset: " + wordset.size + " Goodwords: " + goodwordweight);

    
	if ( hadbadword1 && hadbadword2 >= 2 && ( (wordset.size - goodwordweight) >= 2) )
	//if (message.content.match(badwords))
    	{
    	    if ( !message.member.roles.some(r=>["Developer", "Operator", "test"].includes(r.name)) )
    	    {
    	        message.reply("Any discussion of torrenting, piracy, or other illegitimate means of obtaining software is not allowed on this server. (This is an automated response based on the words you used and can be triggered accidentally.)");
    	    }
    	    
    	    return;
    	}


    // Ignore messages not starting with prefix
    if( !message.content.startsWith(prefix)) return;

    if (message.content.startsWith(prefix + "ping"))
        {
        message.channel.send("pong");
    }

var parseme = message.content;
if (parseme.startsWith(prefix + "faq"))
{
        parseme = parseme.substr(5);
        //TODO: Only show this if no text follows
        if (parseme == "" || parseme.startsWith("help"))
        {
            message.channel.send(
                "Franzbot version: 2019-May-29" + "\n" +
                "Please try one of these fascinating topics:" + "\n" +
				"Type `!faq <topic>` to display the content." + "\n" +
                "help; wiki; status; compile; config; client; vs; wamp; paru; md5"
            );
            return;
        }
        /*else if (parseme.startsWith("help"))
        {
            message.channel.send(
                "Franzbot version: higher than that REST server attempt" + "\n" +
                "FAQ Topics:" + "\n" +
                "Project Wiki:" + " <http://ffxivclassic.fragmenterworks.com/wiki/index.php/Main_Page>" + "\n" +
                "Compiling FFXIV Classic [compile]:" + "Developers: <https://wiki.ffxivrp.org/pages/Compiling> Users: <https://wiki.ffxivrp.org/pages/Compiling-Simple>" + "\n" +
                "Server Configuration [config]:" + " <https://wiki.ffxivrp.org/pages/ServerSetup>" + "\n" +
                "Client Configuration [client]:" + " <https://wiki.ffxivrp.org/pages/ClientAndLauncher> " + "\n" +
                "Visual Studio [vs]:" + " <https://visualstudio.microsoft.com/downloads/>" + "\n" +
                "WAMP and other tools [wamp]: " + " <http://www.wampserver.com/en/#download-wrapper> <http://www.heidisql.com/> " + "\n" +
                "Easy mode [paru]: <http://ffxiv.sqnya.se/>"
            );
            return;
        }*/
        else if (parseme.startsWith("wiki"))
        {
            message.channel.send("Project Wiki:" + " <http://ffxivclassic.fragmenterworks.com/wiki/index.php/Main_Page>");
        }
        else if (parseme.startsWith("compile"))
        {
            message.channel.send("There are two guides for compiling FFXIV Classic.");
            message.channel.send("If you plan on editing the source code or contributing to development, <https://wiki.ffxivrp.org/pages/Compiling>\n" +
            "If you just want to run the servers, <https://wiki.ffxivrp.org/pages/Compiling-Simple>");
        }
        else if (parseme.startsWith("config"))
        {
            message.channel.send("To configure the various FFXIV Servers, import the database, and set up the webserver, please follow the steps on <https://wiki.ffxivrp.org/pages/ServerSetup>");
        }
        else if (parseme.startsWith("client"))
        {
            message.channel.send("You will need to supply your own copy of FFXIV 1.0. The Seventh Umbral Launcher (7U) will patch it up to 1.23b. To configure 7U to connect to a server, please follow the steps on <https://wiki.ffxivrp.org/pages/ClientAndLauncher>");
        }
        else if (parseme.startsWith("vs"))
        {
            message.channel.send("You can download the latest version of Visual Studio or just the build tools here: <https://visualstudio.microsoft.com/downloads/>");
        }
        else if (parseme.startsWith("wamp") || parseme.startsWith("heidi") || parseme.startsWith("tool"))
        {
            message.channel.send("Here are some tools you may find useful.\n");
            message.channel.send(
                "HeidiSQL, a graphic MySQL client: <http://www.heidisql.com/>\n" +
                "SourceTree, a graphical Git client: <https://www.sourcetreeapp.com/>" +
                "WAMP, an all-in-one database and webserver: <http://www.wampserver.com/en/#download-wrapper>\n" +
                ""
            );
        }
        else if (parseme.startsWith("paru"))
        {
            message.channel.send("Paru has a server compiled and running a recent develop branch. Login and instructions are available on <https://7ur.sqnya.se/index.php>");
        }
        else if (parseme.startsWith("progress") || parseme.startsWith("status"))
        {
            message.channel.send("This is difficult to quantify. Chances are, whatever feature you're hoping to have implemented isn't quite ready yet for any variety of reasons. These sorts of questions cannot be answered clearly or with a percentage number and are often between varying degrees of implemented to no plans to implement.");
            message.channel.send(
                "```diff" + "\n" +
                "+ Logging in: implemented" + "\n" +
		"+ Character creation and deletion: implemented" + "\n" +
                "+ Multiple players in zones: implemented" + "\n" +
                "+ Chatting: mostly implemented (linkshells tba)" + "\n" +
                "+ Parties: implemented" + "\n" + 
		"```"
            );
            message.channel.send(
                "```fix" + "\n" +
                "Combat: base code in progress" + "\n" +
		"Monster AI: base code in progress" + "\n" +
		"Monster Pathfinding: base code in progress; navmeshes not generated" + "\n" +
                "Equipping gear: graphics IDs are linked." + "\n" +
                "Leveling up: requires debug commands at this time" + "\n" +
                "```"
            );
            message.channel.send(
                "```diff" + "\n" +
                "- Gear Stats: not implemented" + "\n" +
		"- Monster Spawnpoints: supported, but not implemented" + "\n" +
                "- Quests: generally not implemented" + "\n" +
                "- Crafting and Gathering: not implemented```"
            );

        }
		else if (parseme.startsWith("md5") || parseme.startsWith("sha"))
        {
            message.channel.send("Here is some information about the official 1.0 install disc if you'd like to verify that yours is not damaged or tampered with.");
			message.channel.send(
				"```" + "\n" + 
				"SHA1" + "\n" +
				"3df40d83b227484a302d3f51615d1d90f419daf6" + "\n" +
				"MD5" + "\n" +
				"acb251ad6d246ff1f1d68b6c6c78b234```"
			);

        }
        else
        {
            message.channel.send("Faq not found for '" + parseme + "'");
            return;
        }
    }
    else if (parseme.startsWith(prefix + "command"))
    {
        message.channel.send("This hasn't been implemented yet :<");
        return;
    }
	// Admin commands
	else if (parseme.startsWith(prefix + "diepls"))
	{
		if ( message.member.roles.some(r=>["Developer", "Operator", "test"].includes(r.name)) )
		{
			message.channel.send("Shutting down. [Please use responsibly]");
			return process.exit(0);
		}
		else
		{
			message.channel.send("Error: You lack the permission role to run this command.");
		}
		
	}
	else if (parseme.startsWith(prefix + "restartparu"))
	{
		if ( message.member.roles.some(r=>["Developer", "Operator", "test"].includes(r.name)) )
		{
			message.channel.send("Restarting Paru's server.");
			var request = require('request');
			request(paruserverurl, function (error, response, body)
			{
				if (!error && response.statusCode == 200 && body!="No.") 
				{
					//console.log(body);
					message.channel.send("`" + body + "`"); // Show the HTML
				}
				else 
				{
					//console.log("Error "+response.statusCode);
					message.channel.send("`There was an error running this command.`");
				}
			});
		}
		else
		{
			message.channel.send("Error: You lack the permission role to run this command.");
		}
		
		return;
	}
    

});

client.login(apikey);

